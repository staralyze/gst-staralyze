# GStreamer plugins for the Staralyze project

These plugins are some of the building blocks of the Staralyze projects:
- rsstacking: Is used for stacking the X last images to reduce noise and make stars pop
- rsleveling: Is used to pick one of the component (RGB) and rescale it so that the dark become darker and the white become whiter (turns the image to grayscale)

## Requirements

TODO

## Building

First build the library:

```
cargo build --release
```

Then you need to tell Gstreamer where are the plugins you just built:

```
export GST_PLUGIN_PATH=`pwd`/target/release
```

You can then test the plugins:

```
gst-launch-1.0 videotestsrc ! videoconvert ! rsstacking size=20 ! rsleveling ! videoconvert ! autovideosink
```

## Contributing

Contributors are encouraged to submit merge requests or file bugs on [Gitlab](https://gitlab.com/staralyze/gst-staralyze/-/issues).

## License
gst-staralyze is licensed under GNU General Public License version 3. See the [LICENSE](https://gitlab.com/staralyze/gst-staralyze/-/blob/main/LICENSE) file.

