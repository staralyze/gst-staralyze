/*
 *  Copyright © 2022 Staralyze
 *
 *  This file is part of gst-staralyze.
 *
 *  gst-staralyze is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  gst-staralyze is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with gst-staralyze.  If not, see <https://www.gnu.org/licenses/>.
 */
fn main() {
    gst_plugin_version_helper::info()
}
