/*
 *  Copyright © 2022 Staralyze
 *
 *  This file is part of gst-staralyze.
 *
 *  gst-staralyze is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  gst-staralyze is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with gst-staralyze.  If not, see <https://www.gnu.org/licenses/>.
 */
use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst_base::subclass::prelude::*;
use gst_video::subclass::prelude::*;

use std::i32;
use std::sync::Mutex;

use itertools::izip;

use once_cell::sync::Lazy;

// This module contains the private implementation details of our element
//
static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "rsstacking",
        gst::DebugColorFlags::BG_BLACK | gst::DebugColorFlags::FG_RED,
        Some("Rust Image stacking"),
    )
});

// Default values of properties
const DEFAULT_SIZE: u32 = 50;

// Property value storage
#[derive(Debug, Clone, Copy)]
struct Settings {
    size: u32
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            size: DEFAULT_SIZE
        }
    }
}

// Struct containing all the element data
#[derive(Default)]
pub struct Stacking {
    settings: Mutex<Settings>,
    stack: Mutex<Vec<Vec<u8>>>,
    r_sums: Mutex<Vec<Vec<u32>>>,
    g_sums: Mutex<Vec<Vec<u32>>>,
    b_sums: Mutex<Vec<Vec<u32>>>
}

impl Stacking {
}

// This trait registers our type with the GObject object system and
// provides the entry points for creating a new instance and setting
// up the class data
#[glib::object_subclass]
impl ObjectSubclass for Stacking {
    const NAME: &'static str = "RsStacking";
    type Type = super::Stacking;
    type ParentType = gst_video::VideoFilter;

    fn with_class(_klass: &Self::Class) -> Self {
        // Return an instance of our struct
        Self {
            settings: Mutex::new(Settings::default()),
            stack: Mutex::new(vec![]),
            r_sums: Mutex::new(vec![vec![0; 640]; 480]),
            g_sums: Mutex::new(vec![vec![0; 640]; 480]),
            b_sums: Mutex::new(vec![vec![0; 640]; 480])
        }
    }
}

// Implementation of glib::Object virtual methods
impl ObjectImpl for Stacking {
    fn properties() -> &'static [glib::ParamSpec] {
        // Metadata for the properties
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecUInt::new(
                    "size",
                    "size",
                    "number of image in the stack",
                    0,
                    1000,
                    DEFAULT_SIZE,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_PLAYING,
                ),
            ]
        });

        PROPERTIES.as_ref()
    }

    // Called whenever a value of a property is changed. It can be called
    // at any time from any thread.
    fn set_property(
        &self,
        obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.name() {
            "size" => {
                let mut settings = self.settings.lock().unwrap();
                let size = value.get().expect("type checked upstream");
                gst::info!(
                    CAT,
                    obj: obj,
                    "Changing size from {} to {}",
                    settings.size,
                    size
                );
                settings.size = size;
            }
            _ => unimplemented!(),
        }
    }

    // Called whenever a value of a property is read. It can be called
    // at any time from any thread.
    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "size" => {
                let settings = self.settings.lock().unwrap();
                settings.size.to_value()
            }
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        // Call the parent class' ::constructed() implementation first
        self.parent_constructed(obj);

    }

}

impl GstObjectImpl for Stacking {}

// Implementation of gst::Element virtual methods
impl ElementImpl for Stacking {
    // Set the element specific metadata. This information is what
    // is visible from gst-inspect-1.0 and can also be programatically
    // retrieved from the gst::Registry after initial registration
    // without having to load the plugin in memory.
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "Image stacking",
                "Filter/Effect/Converter/Video",
                "Stacks last X images",
                "Nabos <nabos@glargh.fr>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    // Create and add pad templates for our sink and source pad. These
    // are later used for actually creating the pads and beforehand
    // already provide information to GStreamer about all possible
    // pads that could exist for this type.
    //
    // Our element here can convert BGRx to BGRx or GRAY8, both being grayscale.
    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            // On the src pad, we can produce BGRx and GRAY8 of any
            // width/height and with any framerate
            let caps = gst::Caps::builder("video/x-raw")
                .field(
                    "format",
                    gst::List::new([
                        gst_video::VideoFormat::Bgrx.to_str(),
                    ]),
                )
                .field("width", gst::IntRange::new(0, i32::MAX))
                .field("height", gst::IntRange::new(0, i32::MAX))
                .field(
                    "framerate",
                    gst::FractionRange::new(
                        gst::Fraction::new(0, 1),
                        gst::Fraction::new(i32::MAX, 1),
                    ),
                )
                .build();
            // The src pad template must be named "src" for basetransform
            // and specific a pad that is always there
            let src_pad_template = gst::PadTemplate::new(
                "src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            // On the sink pad, we can accept BGRx of any
            // width/height and with any framerate
            let caps = gst::Caps::builder("video/x-raw")
                .field("format", gst_video::VideoFormat::Bgrx.to_str())
                .field("width", gst::IntRange::new(0, i32::MAX))
                .field("height", gst::IntRange::new(0, i32::MAX))
                .field(
                    "framerate",
                    gst::FractionRange::new(
                        gst::Fraction::new(0, 1),
                        gst::Fraction::new(i32::MAX, 1),
                    ),
                )
                .build();
            // The sink pad template must be named "sink" for basetransform
            // and specific a pad that is always there
            let sink_pad_template = gst::PadTemplate::new(
                "sink",
                gst::PadDirection::Sink,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            vec![src_pad_template, sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }
}

// Implementation of gst_base::BaseTransform virtual methods
impl BaseTransformImpl for Stacking {
    // Configure basetransform so that we are never running in-place,
    // don't passthrough on same caps and also never call transform_ip
    // in passthrough mode (which does not matter for us here).
    //
    // We could work in-place for BGRx->BGRx but don't do here for simplicity
    // for now.
    const MODE: gst_base::subclass::BaseTransformMode =
        gst_base::subclass::BaseTransformMode::NeverInPlace;
    const PASSTHROUGH_ON_SAME_CAPS: bool = false;
    const TRANSFORM_IP_ON_PASSTHROUGH: bool = false;

    // Called for converting caps from one pad to another to account for any
    // changes in the media format this element is performing.
    //
    // In our case that means that:
    fn transform_caps(
        &self,
        element: &Self::Type,
        direction: gst::PadDirection,
        caps: &gst::Caps,
        filter: Option<&gst::Caps>,
    ) -> Option<gst::Caps> {
        let other_caps = if direction == gst::PadDirection::Src {
            // For src to sink, no matter if we get asked for BGRx or GRAY8 caps, we can only
            // accept corresponding BGRx caps on the sinkpad. We will only ever get BGRx and GRAY8
            // caps here as input.
            let mut caps = caps.clone();

            for s in caps.make_mut().iter_mut() {
                s.set("format", &gst_video::VideoFormat::Bgrx.to_str());
            }

            caps
        } else {
            // For the sink to src case, we will only get BGRx caps and for each of them we could
            // output the same caps or the same caps as GRAY8. We prefer GRAY8 (put it first), and
            // at a later point the caps negotiation mechanism of GStreamer will decide on which
            // one to actually produce.
            let mut caps = caps.clone();

            for s in caps.make_mut().iter_mut() {
                s.set("format", &gst_video::VideoFormat::Bgrx.to_str());
            }

            caps
        };

        gst::debug!(
            CAT,
            obj: element,
            "Transformed caps from {} to {} in direction {:?}",
            caps,
            other_caps,
            direction
        );

        // In the end we need to filter the caps through an optional filter caps to get rid of any
        // unwanted caps.
        if let Some(filter) = filter {
            Some(filter.intersect_with_mode(&other_caps, gst::CapsIntersectMode::First))
        } else {
            Some(other_caps)
        }
    }
}

impl VideoFilterImpl for Stacking {
    // Does the actual transformation of the input buffer to the output buffer
    fn transform_frame(
        &self,
        _element: &Self::Type,
        in_frame: &gst_video::VideoFrameRef<&gst::BufferRef>,
        out_frame: &mut gst_video::VideoFrameRef<&mut gst::BufferRef>,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        // Keep a local copy of the values of all our properties at this very moment. This
        // ensures that the mutex is never locked for long and the application wouldn't
        // have to block until this function returns when getting/setting property values
        let settings = *self.settings.lock().unwrap();

        // Keep the various metadata we need for working with the video frames in
        // local variables. This saves some typing below.
        let width = in_frame.width() as usize;
        let height = in_frame.height() as usize;
        let in_stride = in_frame.plane_stride()[0] as usize;
        let in_data = in_frame.plane_data(0).unwrap();
        let out_stride = out_frame.plane_stride()[0] as usize;
        let out_data = out_frame.plane_data_mut(0).unwrap();

        let stack = &mut *self.stack.lock().unwrap();
        stack.push(in_data.to_vec());
        stack.rotate_right(1);


        assert_eq!(in_data.len() % 4, 0);
        assert_eq!(out_data.len() % 4, 0);
        assert_eq!(out_data.len() / out_stride, in_data.len() / in_stride);

        let in_line_bytes = width * 4;
        let out_line_bytes = width * 4;

        assert!(in_line_bytes <= in_stride);
        assert!(out_line_bytes <= out_stride);

        let r_sums = &mut *self.r_sums.lock().unwrap();
        let g_sums = &mut *self.g_sums.lock().unwrap();
        let b_sums = &mut *self.b_sums.lock().unwrap();

        //Substract images that leaves the stack
        if stack.len() > settings.size as usize {
            for b in settings.size as usize..stack.len() {
                for (line, r_sums_line, g_sums_line, b_sums_line) in izip!(
                    stack[b].chunks_exact(in_stride),
                    r_sums.iter_mut(),
                    g_sums.iter_mut(),
                    b_sums.iter_mut())
                {
                    for (p, r_sum, g_sum, b_sum) in izip!(
                        line[..in_line_bytes].chunks_exact(4),
                        r_sums_line.iter_mut(),
                        g_sums_line.iter_mut(),
                        b_sums_line.iter_mut())
                    {
                        *r_sum -= p[0] as u32;
                        *g_sum -= p[1] as u32;
                        *b_sum -= p[2] as u32;
                    }
                }
            }
        }
        stack.truncate(settings.size as usize);

        //Add image that enters the stack
        for (in_line, r_sums_line, g_sums_line, b_sums_line) in izip!(
            in_data.chunks_exact(in_stride),
            r_sums.iter_mut(),
            g_sums.iter_mut(),
            b_sums.iter_mut())
        {
            for (p, r_sum, g_sum, b_sum) in izip!(
                in_line[..in_line_bytes].chunks_exact(4),
                r_sums_line.iter_mut(),
                g_sums_line.iter_mut(),
                b_sums_line.iter_mut())
            {
                *r_sum += p[0] as u32;
                *g_sum += p[1] as u32;
                *b_sum += p[2] as u32;
            }
        }

        for (out_line, r_sums_line, g_sums_line, b_sums_line) in izip!(
            out_data.chunks_exact_mut(out_stride),
            r_sums.iter(),
            g_sums.iter(),
            b_sums.iter())
        {
            for (out_p, r_sum, g_sum, b_sum) in izip!(
                out_line[..out_line_bytes].chunks_exact_mut(4),
                r_sums_line.iter(),
                g_sums_line.iter(),
                b_sums_line.iter())
            {
                assert_eq!(out_p.len(), 4);
                out_p[0] = (r_sum / stack.len() as u32) as u8;
                out_p[1] = (g_sum / stack.len() as u32) as u8;
                out_p[2] = (b_sum / stack.len() as u32) as u8;
            }
        }

        Ok(gst::FlowSuccess::Ok)
    }
}
