/*
 *  Copyright © 2022 Staralyze
 *
 *  This file is part of gst-staralyze.
 *
 *  gst-staralyze is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  gst-staralyze is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with gst-staralyze.  If not, see <https://www.gnu.org/licenses/>.
 */
#![allow(clippy::non_send_fields_in_send_ty)]

use gst::glib;

mod stacking;
mod leveling;

fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    stacking::register(plugin)?;
    leveling::register(plugin)?;
    Ok(())
}

gst::plugin_define!(
    staralyze,
    env!("CARGO_PKG_DESCRIPTION"),
    plugin_init,
    concat!(env!("CARGO_PKG_VERSION")),
    "MIT",
    env!("CARGO_PKG_NAME"),
    env!("CARGO_PKG_NAME"),
    env!("CARGO_PKG_REPOSITORY"),
    env!("BUILD_REL_DATE")
);
